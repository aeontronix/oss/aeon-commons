package com.aeontronix.commons.xml;

import com.aeontronix.commons.exception.UnexpectedException;

public class UnexpectedXmlException extends UnexpectedException {
    public UnexpectedXmlException() {
    }

    public UnexpectedXmlException(String message) {
        super(message);
    }

    public UnexpectedXmlException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnexpectedXmlException(Throwable cause) {
        super(cause);
    }
}
