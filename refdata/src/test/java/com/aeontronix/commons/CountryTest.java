package com.aeontronix.commons;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CountryTest {
    @Test
    public void testCountryToCodeMapping() {
        assertEquals(Country.getCountryName("fr"), "FRANCE");
    }
}
