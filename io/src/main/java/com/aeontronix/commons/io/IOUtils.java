/*
 * Copyright (c) 2016 Kloudtek Ltd
 */

package com.aeontronix.commons.io;

import java.io.*;

/**
 * Various I/O relation utilities
 */
public class IOUtils {
    private static final int DEF_BUFF_SIZE = 10240;
    private static final int DEF_CHAR_BUFF_SIZE = 200;

    public static byte[] toByteArray(InputStream inputStream) throws IOException {
        ByteArrayDataOutputStream buffer = new ByteArrayDataOutputStream();
        copy(inputStream, buffer);
        buffer.close();
        return buffer.toByteArray();
    }

    public static byte[] toByteArray(DataGenerator dataGenerator) throws IOException {
        ByteArrayDataOutputStream buffer = new ByteArrayDataOutputStream();
        dataGenerator.generateData(buffer);
        close(buffer);
        return buffer.toByteArray();
    }

    public static long copy(final InputStream inputStream, final OutputStream outputStream) throws IOException {
        return copy(inputStream, outputStream, DEF_BUFF_SIZE);
    }

    public static long copy(final InputStream inputStream, final OutputStream outputStream, DataCopyListener listener) throws IOException {
        return copy(inputStream, outputStream, DEF_BUFF_SIZE, listener);
    }

    public static long copy(InputStream inputStream, OutputStream outputStream, int bufSize) throws IOException {
        return copy(inputStream, outputStream, bufSize, null);
    }

    @SuppressWarnings("Duplicates")
    public static long copy(InputStream inputStream, OutputStream outputStream, int bufSize, DataCopyListener listener) throws IOException {
        byte[] buffer = new byte[bufSize];
        long count = 0;
        while (true) {
            int read = inputStream.read(buffer);
            if (read > 0) {
                if (listener != null) {
                    listener.dataCopied(buffer, read);
                }
                outputStream.write(buffer, 0, read);
                count += read;
            } else {
                return count;
            }
        }
    }

    public static long copy(final Reader reader, final Writer writer) throws IOException {
        return copy(reader, writer, DEF_CHAR_BUFF_SIZE, null);
    }

    @SuppressWarnings("Duplicates")
    private static long copy(final Reader reader, final Writer writer, int bufSize) throws IOException {
        return copy(reader, writer, bufSize);
    }

    @SuppressWarnings("Duplicates")
    private static long copy(final Reader reader, final Writer writer, int bufSize, DataCopyListener listener) throws IOException {
        char[] buffer = new char[bufSize];
        long count = 0;
        while (true) {
            int read = reader.read(buffer);
            if (read > 0) {
                writer.write(buffer, 0, read);
                count += read;
            } else {
                return count;
            }
        }
    }

    public static String toString(InputStream inputStream) throws IOException {
        return toString(inputStream, "UTF-8");
    }

    public static String toString(InputStream inputStream, String encoding) throws IOException {
        return toString(new InputStreamReader(inputStream, encoding));
    }

    public static String toString(Reader reader) throws IOException {
        StringWriter buffer = new StringWriter();
        copy(reader, buffer);
        return buffer.toString();
    }

    /**
     * Iterate through provided parameters, and close any which implement AutoCloseable
     *
     * @param objects Closeable objects which can be null (in which case nothing will be done)
     */
    public static void close(Object... objects) {
        for (Object object : objects) {
            if (object instanceof AutoCloseable) {
                try {
                    ((AutoCloseable) object).close();
                } catch (Throwable e) {
                    //
                }
            }
        }
    }

    public interface DataGenerator {
        void generateData(OutputStream os) throws IOException;
    }

    public interface DataCopyListener {
        void dataCopied(byte[] data, int len) throws IOException;
    }
}
