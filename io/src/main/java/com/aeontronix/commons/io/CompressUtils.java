package com.aeontronix.commons.io;

import com.aeontronix.commons.exception.UnexpectedException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class CompressUtils {
    public static byte[] gzip(byte[] data) {
        try {
            ByteArrayOutputStream buf = new ByteArrayOutputStream();
            GZIPOutputStream os = new GZIPOutputStream(buf);
            os.write(data);
            os.close();
            return buf.toByteArray();
        } catch (IOException e) {
            throw new UnexpectedException(e);
        }
    }

    public static byte[] gunzip(byte[] gzippedData) throws IOException {
        GZIPInputStream is = new GZIPInputStream(new ByteArrayInputStream(gzippedData));
        byte[] data = IOUtils.toByteArray(is);
        is.close();
        return data;
    }
}
