package com.aeontronix.commons.io;

import com.aeontronix.commons.exception.UnexpectedException;

import java.io.*;
import java.nio.file.Files;
import java.util.*;

@SuppressWarnings("unchecked")
public class BatchQueue<X> {
    private final int overflowStart;
    private final int overflowEnd;
    private final int maxFileSize;
    private int batchSize;
    private final LinkedList<X> queue;
    private final File overflowDir;
    private boolean overflowing;
    private final LinkedList<OverflowFile> openOverflowFiles = new LinkedList<>();
    private final LinkedList<OverflowFile> closedOverflowFiles = new LinkedList<>();

    public BatchQueue(int overflowStart, int overflowEnd, int maxFileSize, int batchSize, File overflowDir) {
        this.overflowStart = overflowStart;
        this.overflowEnd = overflowEnd;
        this.batchSize = batchSize;
        this.queue = new LinkedList<>();
        this.maxFileSize = maxFileSize;
        this.overflowDir = overflowDir;
    }

    public void add(X x) throws IOException {
        OverflowFile overflowFile = null;
        synchronized (this) {
            if (overflowing) {
                Optional<OverflowFile> ovf = openOverflowFiles.stream().filter(f -> !f.locked).findFirst();
                if (ovf.isPresent()) {
                    overflowFile = ovf.get();
                    openOverflowFiles.remove(overflowFile);
                } else {
                    try {
                        overflowFile = new OverflowFile();
                        openOverflowFiles.add(overflowFile);
                    } catch (FileNotFoundException e) {
                        throw new IOException(e);
                    }
                }
            } else {
                queue.add(x);
                if (queue.size() >= overflowStart) {
                    overflowing = true;
                }
            }
        }
        if (overflowFile != null) {
            byte[] data = serialize(x);
            overflowFile.write(data);
            if (overflowFile.file.length() > maxFileSize) {
                overflowFile.stream.close();
                openOverflowFiles.remove(overflowFile);
                closedOverflowFiles.add(overflowFile);
            }
            synchronized (this) {
                overflowFile.locked = false;
            }
        }
    }

    public void process(Processor<X> proc) {
        ArrayList<X> data = new ArrayList<>(batchSize);
        synchronized (this) {
            for (int i = 0; i < batchSize; i++) {
                if( queue.isEmpty() ) {
                    break;
                }
                data.add(queue.removeLast());
            }
            if( queue.size() < overflowEnd ) {
                overflowing = false;
            }
        }
        if (!data.isEmpty()) {
            try {
                proc.process(data);
            } catch (Exception e) {
                synchronized (this) {
                    queue.addAll(data);
                }
            }
        }
    }

    public void processOverflow(Processor<X> func) throws IOException {
        List<X> data;
        synchronized (this) {
            if( ! closedOverflowFiles.isEmpty()) {
                try {
                    OverflowFile ovfile = closedOverflowFiles.getFirst();
                    data = ovfile.read();
                    func.process(data);
                    if( ! ovfile.file.delete() ) {
                        ovfile.file.deleteOnExit();
                    }
                    openOverflowFiles.remove(ovfile);
                    closedOverflowFiles.remove(ovfile);
                } catch (Exception e) {
                    //
                }
            }
        }
    }

    protected byte[] serialize(X x) throws IOException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        ObjectOutputStream os = new ObjectOutputStream(buffer);
        os.writeObject(x);
        os.flush();
        return buffer.toByteArray();
    }

    protected List<X> deserialize( InputStream is ) throws IOException {
        List<X> data = new ArrayList<>();
        try( ObjectInputStream os = new ObjectInputStream(is) ) {
            while (true) {
                data.add((X) os.readObject());
            }
        } catch( EOFException e ) {
            //
        } catch (ClassNotFoundException e) {
            throw new UnexpectedException(e);
        }
        return data;
    }

    private class OverflowFile implements Closeable {
        private final File file;
        private final DataOutputStream stream;
        private boolean locked;

        public OverflowFile() throws IOException {
            file = new File(overflowDir, UUID.randomUUID().toString());
            stream = new DataOutputStream(Files.newOutputStream(file.toPath()));
        }

        @Override
        public void close() throws IOException {
            stream.close();
        }

        public void write( byte[] data ) throws IOException {
            stream.write(data);
        }

        public List<X> read() throws IOException {
            try( DataInputStream is = new DataInputStream(Files.newInputStream(file.toPath())) ) {
                return deserialize(is);
            }
        }
    }

    public interface Processor<X> {
        void process( List<X> data ) throws Exception;
    }
}
