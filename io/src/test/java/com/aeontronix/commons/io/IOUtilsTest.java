package com.aeontronix.commons.io;

import com.aeontronix.commons.StringUtils;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.testng.internal.junit.ArrayAsserts.assertArrayEquals;

public class IOUtilsTest {
    public static final byte[] TESTDATA = StringUtils.utf8("foo");

    @Test
    public void toByteArrayWithGenerator() throws IOException {
        byte[] data = IOUtils.toByteArray(os -> os.write(TESTDATA));
        assertArrayEquals(TESTDATA, data);
    }
}
