package com.aeontronix.commons.io;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class BatchedQueueTest {
    @Test
    public void testOverflow() throws IOException {
        File tempDir = new File("_tmp");
        try {
            Assert.assertTrue(tempDir.mkdir());
            BatchQueue<String> queue = new BatchQueue<>(1, 2, 20, 2, tempDir);
            queue.add("foo1");
            queue.add("foo2");
            queue.add("foo3");
            queue.add("foo4");
            ArrayList<String> expected = new ArrayList<>();
            expected.add("foo1");
            queue.process(s -> Assert.assertEquals(s, expected));
            expected.clear();
            expected.add("foo2");
            expected.add("foo3");
            queue.processOverflow(s -> Assert.assertEquals(s, expected));
            expected.clear();
            expected.add("foo4");
            queue.processOverflow(s -> Assert.assertEquals(s, expected));
            expected.clear();
            queue.processOverflow(s -> Assert.assertTrue(expected.isEmpty()));
            queue.add("foo5");
            expected.clear();
            expected.add("foo5");
            queue.process(s -> Assert.assertEquals(s, expected));
        } finally {
            delete(tempDir);
        }
    }

    private void delete(File file) {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            if (files != null) {
                for (File f : files) {
                    delete(f);
                }
            }
        }
        if (!file.delete()) {
            file.deleteOnExit();
        }
    }
}
