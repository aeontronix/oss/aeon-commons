package com.aeontronix.commons.builder;

import com.aeontronix.commons.file.FileUtils;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.InputStream;

public class FilesBuilderTest {
    @Test
    public void testBuildZip() throws IOException {
        final InputStream is = FileUtils.buildDir().dir("subdir")
                .file("foo.txt").content("FOO")
                .file("bar.txt").content("BAR")
                .buildZipFileAsStream();
    }
}
