package com.aeontronix.commons.file.builder;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

class FileContentMemoryImpl implements FileContent {
    private byte[] data;

    public FileContentMemoryImpl(byte[] data) {
        this.data = data;
    }

    InputStream getInputStream() {
        return new ByteArrayInputStream(data);
    }

    @Override
    public long getSize() {
        return data.length;
    }

    @Override
    public InputStream getDataStream() {
        return new ByteArrayInputStream(data);
    }
}
