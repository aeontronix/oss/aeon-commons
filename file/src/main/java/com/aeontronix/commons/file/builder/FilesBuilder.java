package com.aeontronix.commons.file.builder;

import com.aeontronix.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;

/**
 * This is used to build a file directory
 */
public class FilesBuilder extends FilesBuilderNodeBuilder {
    public FilesBuilder() {
        super(new FilesBuilderDir(null, null), null);
        super.filesBuilder = this;
    }

    public static FilesBuilder build() {
        return new FilesBuilder();
    }

    FileContent createContent(byte[] data) throws IOException {
        return new FileContentMemoryImpl(data);
    }

    FileContent createContent(InputStream data) throws IOException {
        return new FileContentMemoryImpl(IOUtils.toByteArray(data));
    }
}
