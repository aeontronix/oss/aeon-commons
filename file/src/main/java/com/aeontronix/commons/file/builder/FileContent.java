package com.aeontronix.commons.file.builder;

import java.io.InputStream;

interface FileContent {
    long getSize();

    InputStream getDataStream();
}
