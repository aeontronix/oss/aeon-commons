package com.aeontronix.commons.file.builder;

import java.util.LinkedList;

abstract class FilesBuilderNode {
    protected String name;
    protected FilesBuilderDir parent;

    public FilesBuilderNode(String name, FilesBuilderDir parent) {
        this.name = name;
        this.parent = parent;
        if (parent != null) {
            parent.addChildren(this);
        }
    }

    public String getName() {
        return name;
    }

    public FilesBuilderDir getParent() {
        return parent;
    }

    public String getPath(String sep) {
        LinkedList<String> list = new LinkedList<>();
        FilesBuilderNode n = this;
        while (n != null) {
            final String name = n.getName();
            if (name != null) {
                list.addFirst(name);
            }
            n = n.getParent();
        }
        return String.join(sep, list);
    }
}
