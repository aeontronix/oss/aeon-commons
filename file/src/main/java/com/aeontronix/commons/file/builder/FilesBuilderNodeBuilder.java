package com.aeontronix.commons.file.builder;

import com.aeontronix.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class FilesBuilderNodeBuilder {
    protected FilesBuilder filesBuilder;
    protected FilesBuilderNode node;

    public FilesBuilderNodeBuilder(FilesBuilderNode node, FilesBuilder filesBuilder) {
        this.node = node;
        this.filesBuilder = filesBuilder;
    }

    Set<FilesBuilderFile> getFiles() {
        HashSet<FilesBuilderFile> nodes = new HashSet<>();
        LinkedList<FilesBuilderDir> processingQueue = new LinkedList<>();
        if (node instanceof FilesBuilderDir) {
            processingQueue.add((FilesBuilderDir) node);
            while (!processingQueue.isEmpty()) {
                final FilesBuilderDir dir = processingQueue.poll();
                for (FilesBuilderNode node : dir.getChildrens().values()) {
                    if (node instanceof FilesBuilderDir) {
                        processingQueue.add((FilesBuilderDir) node);
                    } else {
                        nodes.add((FilesBuilderFile) node);
                    }
                }
            }
        } else {
            nodes.add((FilesBuilderFile) node);
        }
        return nodes;
    }

    public InputStream buildZipFileAsStream() throws IOException {
        try (final ByteArrayOutputStream buf = new ByteArrayOutputStream(); final java.util.zip.ZipOutputStream out = new ZipOutputStream(buf)) {
            final Set<FilesBuilderFile> nodes = filesBuilder.getFiles();
            for (FilesBuilderFile file : nodes) {
                final ZipEntry zipEntry = new ZipEntry(new ZipEntry(file.getPath("/")));
                final FileContent content = file.getContent();
                zipEntry.setSize(content.getSize());
                out.putNextEntry(zipEntry);
                IOUtils.copy(content.getDataStream(), out);
                out.closeEntry();
            }
            out.finish();
            out.flush();
            return new ByteArrayInputStream(buf.toByteArray());
        }
    }

    public FilesBuilderNodeBuilder dir(String name) {
        FilesBuilderDir parentNode = getDirNode();
        final FilesBuilderDir newDir = new FilesBuilderDir(name, parentNode);
        return new FilesBuilderNodeBuilder(newDir, filesBuilder);
    }

    public FilesBuilderNodeBuilder addZipContent(InputStream is) throws IOException {
        try (ZipInputStream zis = new ZipInputStream(is)) {
            for (ZipEntry entry = zis.getNextEntry(); entry != null; entry = zis.getNextEntry()) {
                final FilesBuilderNodeBuilder fb = deepFile(entry.getName(), "/");
                fb.content(zis);
            }
        }
        return this;
    }

    public FilesBuilderNodeBuilder deepFile(String path, String separator) {
        final LinkedList<String> els = new LinkedList<>(Arrays.asList(path.split(separator)));
        final String name = els.removeLast();
        FilesBuilderDir parentNode = getDirNode();
        for (String el : els) {
            parentNode = new FilesBuilderDir(el, parentNode);
        }
        final FilesBuilderFile file = new FilesBuilderFile(name, parentNode);
        return new FilesBuilderNodeBuilder(file, filesBuilder);
    }

    public FilesBuilderNodeBuilder file(String name) {
        final FilesBuilderDir parentNode = getDirNode();
        final FilesBuilderFile fileNode = new FilesBuilderFile(name, parentNode);
        return new FilesBuilderNodeBuilder(fileNode, filesBuilder);
    }

    private void setContent(FileContent content) {
        if (node instanceof FilesBuilderFile) {
            ((FilesBuilderFile) node).setContent(content);
        } else {
            throw new IllegalStateException("Cannot add content to a directory");
        }
    }

    public FilesBuilderNodeBuilder content(byte[] data) throws IOException {
        setContent(filesBuilder.createContent(data));
        return this;
    }

    public FilesBuilderNodeBuilder content(InputStream data) throws IOException {
        setContent(filesBuilder.createContent(data));
        return this;
    }

    public FilesBuilderNodeBuilder content(String string) throws IOException {
        content(string, StandardCharsets.UTF_8);
        return this;
    }

    public FilesBuilderNodeBuilder content(String string, Charset encoding) throws IOException {
        setContent(filesBuilder.createContent(string.getBytes(encoding)));
        return this;
    }

    public FilesBuilderNodeBuilder file(String name, byte[] data) throws IOException {
        return file(name).content(data);
    }

    private FilesBuilderDir getDirNode() {
        FilesBuilderDir parentNode;
        if (node instanceof FilesBuilderFile) {
            parentNode = node.getParent();
        } else {
            parentNode = (FilesBuilderDir) node;
        }
        return parentNode;
    }
}
