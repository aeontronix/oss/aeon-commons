package com.aeontronix.commons.file.builder;

class FilesBuilderFile extends FilesBuilderNode {
    private FileContent content;

    public FilesBuilderFile(String name, FilesBuilderDir parent) {
        super(name, parent);
    }

    public FileContent getContent() {
        return content;
    }

    public void setContent(FileContent content) {
        this.content = content;
    }
}
