package com.aeontronix.commons.file.builder;

import java.util.HashMap;
import java.util.Map;

class FilesBuilderDir extends FilesBuilderNode {
    public FilesBuilderDir(String name, FilesBuilderDir parent) {
        super(name, parent);
    }

    private final HashMap<String, FilesBuilderNode> childrens = new HashMap<>();

    void addChildren(FilesBuilderNode node) {
        childrens.put(node.getName(), node);
    }

    public Map<String, FilesBuilderNode> getChildrens() {
        return childrens;
    }
}
