package com.aeontronix.commons.properties;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.javaprop.JavaPropsMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator;

import java.io.*;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

public class PropertiesUtils {
    private static final ObjectMapper jsonMapper = new ObjectMapper();
    private static final ObjectMapper yamlMapper = new ObjectMapper(new YAMLFactory().disable(YAMLGenerator.Feature.WRITE_DOC_START_MARKER));
    private static final JavaPropsMapper propsMapper = new JavaPropsMapper();

    public static Map<String, String> toMap(Properties properties) {
        return properties.entrySet().stream().collect(
                Collectors.toMap(
                        e -> String.valueOf(e.getKey()),
                        e -> String.valueOf(e.getValue()),
                        (prev, next) -> next, HashMap::new
                ));
    }

    public static Properties readProperties(File file) throws IOException {
        try (FileInputStream fis = new FileInputStream(file)) {
            return readProperties(fis, getFileExtension(file));
        }
    }

    public static void writeProperties(File file, Properties properties) throws IOException {
        try (final FileOutputStream fos = new FileOutputStream(file)) {
            writeProperties(fos, properties, getFileExtension(file));
        }
    }

    private static void writeProperties(OutputStream outputStream, Properties properties, String ext) throws IOException {
        ext = ext.toLowerCase(Locale.ROOT);
        if (ext.equals("properties")) {
            properties.store(outputStream, null);
        } else if (ext.equals("json")) {
            writeProperties(outputStream, properties, jsonMapper);
        } else if (ext.equals("yaml")) {
            writeProperties(outputStream, properties, yamlMapper);
        } else {
            throw new IllegalArgumentException("Invalid extension: " + ext);
        }
    }

    @SuppressWarnings("rawtypes")
    private static void writeProperties(OutputStream outputStream, Properties properties, ObjectMapper mapper) throws IOException {
        final Map map = propsMapper.readPropertiesAs(properties, Map.class);
        mapper.writerWithDefaultPrettyPrinter().writeValue(outputStream, map);
    }

    private static String getFileExtension(File file) {
        final String fn = file.getName();
        final int i = fn.lastIndexOf('.');
        if (i == -1) {
            throw new IllegalArgumentException("No extension found: " + fn);
        }
        return fn.substring(i + 1);
    }

    private static Properties readProperties(InputStream is, String ext) throws IOException {
        ext = ext.toLowerCase(Locale.ROOT);
        if (ext.equals("properties")) {
            Properties props = new Properties();
            props.load(is);
            return props;
        } else if (ext.equals("json")) {
            return readProperties(is, jsonMapper);
        } else if (ext.equals("yaml") || ext.equals(".yml")) {
            return readProperties(is, yamlMapper);
        } else {
            throw new IllegalArgumentException("Invalid extension: " + ext);
        }
    }

    private static Properties readProperties(InputStream is, ObjectMapper om) throws IOException {
        final JsonNode jsonNode = om.readTree(is);
        return propsMapper.writeValueAsProperties(jsonNode);
    }
}
