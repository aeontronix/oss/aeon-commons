package com.aeontronix.commons.properties;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.Map;
import java.util.Properties;

public class PropertiesUtilsTest {
    private final Properties p;

    public PropertiesUtilsTest() throws IOException {
        p = new Properties();
        p.load(Files.newInputStream(getFile("properties.properties").toPath()));
    }

    @Test
    public void testProperties() throws IOException {
        validateProperties(PropertiesUtils.readProperties(getFile("properties.properties")));
    }

    @Test
    public void testYamlProperties() throws IOException {
        validateProperties(PropertiesUtils.readProperties(getFile("properties.yaml")));
    }

    @Test
    public void testJsonProperties() throws IOException {
        validateProperties(PropertiesUtils.readProperties(getFile("properties.json")));
    }

    @Test
    public void testWriteJsonProperties() throws IOException {
        final File file = outputFile("json");
        PropertiesUtils.writeProperties(file, p);
        assertEquals(file, "properties.json");
    }

    @Test
    public void testWriteYamlProperties() throws IOException {
        final File file = outputFile("yaml");
        PropertiesUtils.writeProperties(file, p);
    }

    private static File outputFile(String ext) {
        final File file = new File("properties/target/test/properties." + ext);
        if (!file.getParentFile().exists()) {
            Assert.assertTrue(file.getParentFile().mkdirs());
        }
        return file;
    }

    private void validateProperties(Properties props) {
        Assert.assertEquals(props, p);
    }

    private File getFile(String name) {
        try {
            return new File(getClass().getResource("/" + name).toURI());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    private void assertEquals(File file, String name) throws IOException {
        Assert.assertEquals(new ObjectMapper().readValue(file, Map.class),
                new ObjectMapper().readValue(getFile(name), Map.class));
    }
}
