package com.aeontronix.commons.validation;

import jakarta.validation.constraints.NotBlank;

public class TestObj {
    @NotBlank
    private String value;

    public TestObj(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
