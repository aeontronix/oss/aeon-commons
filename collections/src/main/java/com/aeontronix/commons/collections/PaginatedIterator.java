package com.aeontronix.commons.collections;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * This class is used to convert a paginated list into an iterator.
 */
public class PaginatedIterator<X> implements Iterator<X> {
    private PaginationSupplier<X> supplier;
    private Iterator<X> iterator;

    public PaginatedIterator(PaginationSupplier<X> supplier) {
        this.supplier = supplier;
        iterator = supplier.get();
    }

    public List<X> toList() {
        return StreamSupport
                .stream(((Iterable<X>) () -> PaginatedIterator.this).spliterator(), false)
                .collect(Collectors.toList());
    }

    @Override
    public boolean hasNext() {
        final boolean hasNext = iterator.hasNext();
        if (!hasNext && supplier.fetchNextPage()) {
            iterator = supplier.get();
            return true;
        } else {
            return hasNext;
        }
    }

    @Override
    public X next() {
        if (!hasNext()) {
            throw new NoSuchElementException();
        }
        return iterator.next();
    }
}
