package com.aeontronix.commons.collections;

import java.util.Iterator;
import java.util.function.Supplier;

public interface PaginationSupplier<X> extends Supplier<Iterator<X>> {
    /**
     * Fetches next page of data.
     *
     * @return true if more data is available
     */
    boolean fetchNextPage();
}
