package com.aeontronix.commons.collections;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class PaginatedIteratorTest {
    @Test
    public void testPaginatedIterator() {
        final List<String> l1 = Arrays.asList("1", "2");
        final List<String> l2 = Arrays.asList("3", "4");
        final PaginationSupplier<String> s = new PaginationSupplier<String>() {
            public List<String> ls;

            @Override
            public Iterator<String> get() {
                if (ls == null) {
                    ls = l1;
                }
                return ls.iterator();
            }

            @Override
            public boolean fetchNextPage() {
                if (ls == l1) {
                    ls = l2;
                    return true;
                } else {
                    return false;
                }
            }
        };
        final List<String> list = new PaginatedIterator<>(s).toList();
        Assert.assertEquals(list, Arrays.asList("1", "2", "3", "4"));
    }
}
