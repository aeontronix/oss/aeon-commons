package com.aeontronix.commons;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by yannick on 14/3/16.
 */
public class URLBuilderTest {
    @Test
    public void testBuildUrlNoQuery() {
        String url = new URLBuilder("http://site?x=y+y").path("foo").path("bar").path("/buz/").path("baz/").path("/boz").toString();
        Assert.assertEquals(url, "http://site/foo/bar/buz/baz/boz?x=y+y");
    }

    @Test
    public void testBuildOverrideParam() {
        String url = new URLBuilder("http://site/xx?foo=bar").queryParam("foo", "xpt").toString();
        Assert.assertEquals(url, "http://site/xx?foo=xpt");
    }

    @Test
    public void testBuildAddParam() {
        String url = new URLBuilder("http://site/xx?foo=bar").queryParam("foo", "xpt", true, true).toString();
        Assert.assertEquals(url, "http://site/xx?foo=bar&foo=xpt");
    }

    @Test
    public void testBuildUrlWithQueryParams() {
        String url = new URLBuilder("http://site/foo").path("bar").queryParam("key1", "val1").queryParam("key2", "val2").setUserInfo("bahamut").setRef("boo").toString();
        Assert.assertEquals(url, "http://bahamut@site/foo/bar#boo?key1=val1&key2=val2");
    }

    @Test
    public void testBuildUrlWithQueryParamsFromRelative() {
        String url = new URLBuilder("foo").path("bar").queryParam("key1", "val1").queryParam("key2", "val2").setUserInfo("bahamut")
                .setRef("boo").setHost("mysite").setPort(33).setProtocol("https").toString();
        Assert.assertEquals(url, "https://bahamut@mysite:33/foo/bar#boo?key1=val1&key2=val2");
    }

    @Test
    public void testBuildUrlWithInlineQuery() {
        String url = new URLBuilder("http://site/foo").queryParam("key1", "val1").path("bar?key2=val2&key3=val3").queryParam("key4", "val4").path("baz").toString();
        Assert.assertEquals(url, "http://site/foo/bar/baz?key1=val1&key2=val2&key3=val3&key4=val4");
    }

    @Test
    public void testBuildRelative() {
        String url = new URLBuilder("/foo").queryParam("key1", "val1").path("bar?key2=val2&key3=val3").queryParam("key4", "val4").path("baz").toString();
        Assert.assertEquals(url, "/foo/bar/baz?key1=val1&key2=val2&key3=val3&key4=val4");
    }

    @Test
    public void testBuildRelativeWithEncodedParams() {
        String url = new URLBuilder("/foo?k=x+x").queryParam("key1", "Hello%25%22%2F~", false, true)
                .path("bar?key2=val2&key3=val3").queryParam("key4", "X%@#$%!(*\"").path("baz").toString();
        url = new URLBuilder(new URLBuilder("http://boo").path(url).path("gah").queryParam("bla", "X\"X").toString()).toString();
        Assert.assertEquals(url,
                "http://boo/foo/bar/baz/gah?k=x+x&key1=Hello%25%22%2F~&key2=val2&key3=val3&key4=X%25%40%23%24%25%21%28*%22&bla=X%22X");
    }

    @Test
    public void testBuildWithFragment() {
        String url = new URLBuilder("http://boo/lok#rev?qq=rr").path("xoy").queryParam("groo", "obp").toString();
        Assert.assertEquals(url, "http://boo/lok/xoy#rev?qq=rr&groo=obp");
    }

    @Test
    public void testBuildWithFragmentInterMarkNoQParam() {
        String url = new URLBuilder("http://boo/lok#rev?").path("xoy").queryParam("groo", "obp").toString();
        Assert.assertEquals(url, "http://boo/lok/xoy#rev?groo=obp");
    }

    @Test
    public void setNullOverwriteExistingQueryParam() {
        String url = new URLBuilder("http://boo/foo?x=ga").queryParam("x", null).toString();
        Assert.assertEquals(url, "http://boo/foo");
    }

    @Test
    public void setNullCreateNewQueryParam() {
        String url = new URLBuilder("http://boo/foo").queryParam("x", null).toString();
        Assert.assertEquals(url, "http://boo/foo");
    }

    @Test
    public void setNullAppendNewQueryParam() {
        String url = new URLBuilder("http://boo/foo?x=ga").queryParam("x", null, true, true).toString();
        Assert.assertEquals(url, "http://boo/foo?x=ga");
    }

    @Test
    public void setNullAppendAdditionalQueryParam() {
        String url = new URLBuilder("http://boo/foo").queryParam("x", null, true, true).toString();
        Assert.assertEquals(url, "http://boo/foo");
    }

    @Test
    public void testAddFullPathWithQueryParam() {
        String url = new URLBuilder("http://boo/").path("foo/bar?x=y").path("/baz").toString();
        Assert.assertEquals(url, "http://boo/foo/bar/baz?x=y");
    }

    @Test
    public void testAddPathElement() {
        String url = new URLBuilder("http://boo/").pathEl("foo/bar|baz").toURI().toString();
        Assert.assertEquals(url, "http://boo/foo%2Fbar%7Cbaz");
    }
}
