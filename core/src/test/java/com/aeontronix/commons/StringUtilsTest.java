package com.aeontronix.commons;

import org.jetbrains.annotations.NotNull;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;

import static org.testng.Assert.assertEquals;

/**
 * Created by yannick on 15/3/16.
 */
public class StringUtilsTest {
    String decodedPath;
    public static final String PATH = "/\\!@#$%^&*(){}'\"`§てすとｽｾｿﾀ %$&+,/:;=?@<>#%";

    @Test
    public void testBase64Encode() {
        final String encoded = StringUtils.base64EncodeToString("sfadomsdf;oasjfoiasjdf;oasgnu@#%$@%%^9gsaflosajf892346912354jlfkfjasd89f7usad9f732l4kjs98r7987423lku4sfadomsdf;oasjfoiasjdf;oasgnu@#%$@%%^9gsaflosajf892346912354jlfkfjasd89f7usad9f732l4kjs98r7987423lku4", false);
        Assert.assertEquals(encoded, "c2ZhZG9tc2RmO29hc2pmb2lhc2pkZjtvYXNnbnVAIyUkQCUlXjlnc2FmbG9zYWpmODkyMzQ2OTEyMzU0amxma2ZqYXNkODlmN3VzYWQ5ZjczMmw0a2pzOThyNzk4NzQyM2xrdTRzZmFkb21zZGY7b2FzamZvaWFzamRmO29hc2dudUAjJSRAJSVeOWdzYWZsb3NhamY4OTIzNDY5MTIzNTRqbGZrZmphc2Q4OWY3dXNhZDlmNzMybDRranM5OHI3OTg3NDIzbGt1NA");
    }

    @Test
    public void testVarSubSimple() {
        assertEquals("blabarbleh", StringUtils.substituteVariables("bla${foo}bleh", genVars()));
    }

    @Test
    public void testVarSubEscape() {
        assertEquals("bla${foo}bleh", StringUtils.substituteVariables("bla$${foo}bleh", genVars()));
    }

    @Test
    public void testVarSubEscape2() {
        assertEquals("bla$$bleh", StringUtils.substituteVariables("bla$$bleh", genVars()));
    }

    @Test
    public void testVarSubNeverFail() {
        assertEquals("blableh", StringUtils.substituteVariables("bla${xxx}bleh", genVars()));
    }

    @Test
    public void testVarSubNeverFailBadArg() {
        assertEquals("blableh", StringUtils.substituteVariables("bla${s:fsd}bleh", genVars()));
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testVarSubFail() {
        StringUtils.substituteVariables("bla${xxx}bleh", genVars(), false);
    }

    @Test
    public void testVarSubSuffix() {
        assertEquals("bla-bar", StringUtils.substituteVariables("bla${s:-:foo}", genVars()));
    }

    @Test
    public void testVarSubSuffixComma() {
        assertEquals("bla:bar", StringUtils.substituteVariables("bla${s::::foo}", genVars()));
    }

    @Test
    public void testVarSubPrefix() {
        assertEquals("bar-bla", StringUtils.substituteVariables("${p:-:foo}bla", genVars()));
    }

    @Test
    public void testVarSubPrefixComman() {
        assertEquals("bar:bla", StringUtils.substituteVariables("${p::::foo}bla", genVars()));
    }

    @Test
    public void testVarSubSuffixEmpty() {
        assertEquals("bla", StringUtils.substituteVariables("bla${s:-:xxx}", genVars()));
    }

    @Test
    public void testVarSubUpperCase() {
        assertEquals("blaBAR", StringUtils.substituteVariables("bla${u:foo}", genVars()));
    }

    @Test
    public void testVarSubLowerCase() {
        assertEquals("blamoo", StringUtils.substituteVariables("bla${l:boo}", genVars()));
    }

    @Test
    public void testVarCapitalize() {
        assertEquals("blaBar", StringUtils.substituteVariables("bla${c:foo}", genVars()));
    }

    @Test
    public void testVarString() {
        assertEquals("blaBl:~ a", StringUtils.substituteVariables("bla${t:Bl:~ a}", genVars()));
    }

    @Test
    public void testVarInVar() {
        assertEquals("blabarbleh", StringUtils.substituteVariables("bla${${alias}}bleh", genVars()));
    }

    @Test
    public void testEndsWithDollar() {
        assertEquals("bla$", StringUtils.substituteVariables("bla$", genVars()));
    }

    @NotNull
    private HashMap<String, String> genVars() {
        HashMap<String, String> vars = new HashMap<>();
        vars.put("foo", "bar");
        vars.put("boo", "MOO");
        vars.put("alias", "foo");
        return vars;
    }
}
