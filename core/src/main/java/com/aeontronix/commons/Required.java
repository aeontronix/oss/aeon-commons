package com.aeontronix.commons;

/**
 * This enum indicates how to handle missing data. REQUIRED means the data is required to exist and processing should fail,
 * CREATE means the data should be created if missing, OPTIONAL means the data isn't required and process can proceed
 */
public enum Required {
    REQUIRED, CREATE, OPTIONAL
}
