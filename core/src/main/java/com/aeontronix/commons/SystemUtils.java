/*
 * Copyright (c) 2014 Kloudtek Ltd
 */

package com.aeontronix.commons;

/**
 * Various system level utility functions
 */
public class SystemUtils {
    private static final String[] SERVER_TYPE_FILES = new String[]{"/etc/server-type"};
    public static final boolean android;

    static {
        android = System.getProperty("java.vm.name").equalsIgnoreCase("Dalvik");
    }

    public static boolean isAndroid() {
        return android;
    }

    public static boolean hasClass(String className) {
        try {
            Class.forName(className);
            return true;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }
}
