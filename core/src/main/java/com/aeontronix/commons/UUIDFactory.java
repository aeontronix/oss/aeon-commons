/*
 * Copyright (c) 2014 Kloudtek Ltd
 */

package com.aeontronix.commons;

import java.util.UUID;

/**
 * UUID factory class.
 */
public abstract class UUIDFactory {
    public abstract UUID create();
}
