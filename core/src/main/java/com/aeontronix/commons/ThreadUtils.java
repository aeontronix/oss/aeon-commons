/*
 * Copyright (c) 2014 Kloudtek Ltd
 */

package com.aeontronix.commons;

import com.aeontronix.commons.exception.UnexpectedException;

import java.time.Duration;
import java.time.LocalDateTime;

/**
 * Thread-related helper functions
 */
public class ThreadUtils {
    public static void sleep(final long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            throw new UnexpectedException(e);
        }
    }

    public static void sleepUntil(LocalDateTime time) {
        final long sleepDuration = Duration.between(time, LocalDateTime.now()).toMillis();
        if (sleepDuration > 0) {
            ThreadUtils.sleep(sleepDuration);
        }
    }
}
