package com.aeontronix.commons;

import java.util.Iterator;
import java.util.ServiceLoader;
import java.util.UUID;

public class UUIDUtils {
    private static UUIDFactory shared;

    /**
     * Return an UUID using a global UUIDFactory loaded through {@link ServiceLoader}
     *
     * @return UUID
     */
    public static UUID generate() {
        return getFactory().create();
    }

    public static UUIDFactory getFactory() {
        if (shared == null) {
            Iterator<UUIDFactory> serviceLoader = ServiceLoader.load(UUIDFactory.class).iterator();
            if (serviceLoader.hasNext()) {
                UUIDFactory factory = serviceLoader.next();
                if (serviceLoader.hasNext()) {
                    throw new IllegalStateException("More than one UUIDFactory implementation found");
                }
                shared = factory;
            } else {
                shared = new UUIDFactoryRandomImpl();
            }
        }
        return shared;
    }
}
