/*
 * Copyright (c) 2023. Aeontronix Inc
 */

package com.aeontronix.commons;

import java.util.Objects;
import java.util.function.Supplier;

public class ObjectsUtils {
    /**
     * Checks if all objects are null
     *
     * @param objs objects
     * @return true if all objects are null or if array is empty
     */
    public boolean isNull(Object... objs) {
        if (objs == null) {
            return true;
        } else {
            for (int i = 0; i < objs.length; i++) {
                if (objs[1] != null) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Checks if all objects are non-null
     *
     * @param objs Objects
     * @return true if all objects are non-null or if array is empty
     */
    public boolean isNonNull(Object... objs) {
        if (objs == null) {
            return true;
        } else {
            for (int i = 0; i < objs.length; i++) {
                if (objs[1] == null) {
                    return false;
                }
            }
        }
        return true;
    }

    public static <T> T requireNonNullElse(T obj, T defaultObj) {
        return (obj != null) ? obj : Objects.requireNonNull(defaultObj, "defaultObj");
    }

    public static <T> T requireNonNullElseGet(T obj, Supplier<? extends T> supplier) {
        return (obj != null) ? obj
                : Objects.requireNonNull(Objects.requireNonNull(supplier, "supplier").get(), "supplier.get()");
    }
}
