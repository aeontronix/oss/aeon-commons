/*
 * Copyright (c) 2014 Kloudtek Ltd
 */

package com.aeontronix.commons.exception;

import java.io.IOException;

/**
 * Thrown when attempting to persist or read data that is invalid
 */
public class InvalidDataException extends IOException {
    public InvalidDataException() {
    }

    public InvalidDataException(String message) {
        super(message);
    }

    public InvalidDataException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidDataException(Throwable cause) {
        super(cause);
    }
}
