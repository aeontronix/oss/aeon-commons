/*
 * Copyright (c) 2014 Kloudtek Ltd
 */

package com.aeontronix.commons.exception;

/**
 * Exception thrown when provided content is too large
 */
public class DataTooLargeException extends InvalidDataException {
    public DataTooLargeException() {
    }

    public DataTooLargeException(String message) {
        super(message);
    }

    public DataTooLargeException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataTooLargeException(Throwable cause) {
        super(cause);
    }

}
