/*
 * Copyright (c) 2014 Kloudtek Ltd
 */

package com.aeontronix.commons.exception;

/**
 * Created by yannick on 03/01/2014.
 */
public class DataLengthLimitException extends InvalidDataException {
    public DataLengthLimitException() {
    }

    public DataLengthLimitException(String message) {
        super(message);
    }

    public DataLengthLimitException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataLengthLimitException(Throwable cause) {
        super(cause);
    }
}
